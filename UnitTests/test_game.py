from django.test import TestCase
from unittest import TestCase

# TODO
#   WIP: Work in Progress SPRINT II IMPLEMENTATION...

class GameClassTest(TestCase):

    def setUp(self):
        pass

    def test_start(self):
        # this will initialize a new game and generate hands for all players
        pass

    def test_get_high_hand(self):
        # return the highest hand in the game
        pass

    def test_total_pot(self):
        # return the total pot
        pass

    def test_update_total_pot(self, coins):
        pass

    def test_update_winner(self, player):
        # update who wins the pot
        pass

    def test_game_over(self):
        # find the winner and the highest hand
        pass

    def test_display(self):
        # this will display the total pots and the winner for the game
        pass