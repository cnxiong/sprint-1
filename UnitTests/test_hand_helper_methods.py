from unittest import TestCase
from AppATDD.classes import card
from AppATDD.classes import deck
from AppATDD.classes import hand

#  TODO COMPLETED FOR SPRINT I PASSING 100%

class HandTestHelper(TestCase):
    def setUp(self):
        # Example hand with a starting bet of 0
        self.testDeck = deck.DeckClass()
        self.testDeck2 = deck.DeckClass()

        self.hand = hand.HandClass(deck.DeckClass())

        # any Randomly Generated Hand Will Do for These
        self.test_sort_suit = hand.HandClass(deck.DeckClass())
        self.test_sort_rank = hand.HandClass(deck.DeckClass())

        # use manual card arrays to create hands to test
        # 1:clubs 2:spades 3:hearts 4:diamonds
        # initialize a card to the given #suit (1-4) #rank (1-13)
        self.straight_flush = []
        self.royal_flush = []
        self.four_of_a_kind = []
        self.full_house = []
        self.flush = []
        self.straight = []
        self.three_of_a_kind = []
        self.two_pair = []
        self.pair = []
        self.high_card = []

        # use to print whichever array and check it
        # for cardX in self.four_of_a_kind:
        #     print(cardX)

        # ROYAL FLUSH
        Suit = 1
        for Rank in range(10, 14):
            self.card = card.CardClass(Suit, Rank)
            # 1:clubs 10,J,Q,K, .. A
            self.royal_flush.append(self.card)
        self.royal_flush.append(card.CardClass(1, 1))

        # STRAIGHT FLUSH
        Suit = 1
        for Rank in range(2, 7):
            self.card = card.CardClass(Suit, Rank)
            # 1:clubs 2,3,4,5,6
            self.straight_flush.append(self.card)

        # FOUR OF A KIND
        for Suit in range(1, 5):
            Rank = 2
            self.card = card.CardClass(Suit, Rank)
            # 2 OF 1:clubs 2:spades 3:hearts 4:diamonds + AC
            self.four_of_a_kind.append(self.card)
        self.four_of_a_kind.append(card.CardClass(1, 1))

        # FULL HOUSE
        for Suit in range(1, 4):
            for Rank in range(2, 3):
                self.card = card.CardClass(Suit, Rank)
                # 2 OF 1:clubs 2:spades 3:hearts
                self.full_house.append(self.card)
        for Rank in range(3, 4):
            for Suit in range(1, 3):
                self.card= card.CardClass(Suit, Rank)
                self.full_house.append(self.card)
                # other 2 cards are 3 of 1:clubs 2:spades

        # STRAIGHT
        Suit = 1
        for Rank in range(1, 5):
            self.card = card.CardClass(Suit, Rank)
            self.straight.append(self.card)
            Suit= Suit+1
            # 1: clubs A, 5 2:spades 2 3:hearts 3 4:diamonds 4
            # 1,2,3,4 alternating suits, 5 back to clubs
        self.straight.append(card.CardClass(1, 5))

        # FLUSH
        for Suit in range(1, 2):
            Ranks = [2,4,6,8,10]
            for Rank in Ranks:
                self.card = card.CardClass(Suit, Rank)
                # 1:clubs 2,5,6,8,10
                self.flush.append(self.card)


        # THREE OF A KIND
        for Suit in range(1, 4):
            for Rank in range(2, 3):
                self.card = card.CardClass(Suit, Rank)
                # 2 OF 1:clubs 2:spades 3:hearts  + AC + 7C
                self.three_of_a_kind.append(self.card)
        self.three_of_a_kind.append(card.CardClass(1, 1))
        self.three_of_a_kind.append(card.CardClass(1, 7))

        #TWO PAIR
        Ranks = [2,3]
        for Rank in Ranks:
            Suits = [1,2]
            for Suit in Suits:
                self.card = card.CardClass(Suit, Rank)
                self.two_pair.append(self.card)
                # 2 and 3 OF 1:clubs 2:spades + 7C
        self.two_pair.append(card.CardClass(1, 7))

        # PAIR
        for Rank in range(2, 3):
            for Suit in range(1, 3):
                self.card = card.CardClass(Suit, Rank)
                self.pair.append(self.card)
                # pair 2 of 1:clubs 2:spades
        self.pair.append(card.CardClass(1, 1))
        self.pair.append(card.CardClass(1, 3))
        self.pair.append(card.CardClass(1, 4))
        # clubs 1,3,4 to fill hand

        #HIGH CARD
        for Suit in range(1, 2):
            for Rank in range(1, 5):
                self.card = card.CardClass(Suit, Rank)
                self.high_card.append(self.card)
                # clubs 1,2,3,4
        self.high_card.append(card.CardClass(2, 7))
        # break the streak with high card 7S

    def test_sortRank(self):
        self.test_sort_rank.sortRank()
        prev = self.test_sort_rank.cards[0].getRank()
        self.test_sort_rank.__str__()
        print("TEST SORTING RANK")
        # Make sure all the cards are sorted in numerical order
        for cardX in self.test_sort_rank.cards:
            self.assertLessEqual(prev, cardX.getRank())
            prev = cardX.getRank()

    def test_sortSuit(self):
        self.test_sort_suit.sortSuit()
        prev = self.test_sort_suit.cards[0].getSuit()
        self.test_sort_suit.__str__()
        print("TEST SORTING SUIT")
        # Make sure all the cards are sorted in numerical order
        for cardX in self.test_sort_suit.cards:
            self.assertLessEqual(prev, cardX.getSuit())
            prev = cardX.getSuit()

    def test_empty_hand(self):
        self.hand.empty_hand()
        self.assertEqual(self.hand.hand_value, ['NONE', None, 0])
        self.assertEqual(self.hand.cards.__len__(), 0)

    def test_is_royal(self):
        # now I have a hand with a royal flush
        self.hand.overwrite_hand(self.royal_flush)
        self.hand.__str__()
        # check in hand class that the calculate value method returns
        # an array  return ["ROYAL FLUSH", self, 10]
        self.assertEqual(self.hand.calculate_hand_value()[0], "ROYAL FLUSH")
        print(self.hand.calculate_hand_value()[0] + " == ROYAL FLUSH")
        self.assertEqual(self.hand.calculate_hand_value()[2], 10)

    def test_is_fourofakind(self):
        self.hand.overwrite_hand(self.four_of_a_kind)
        self.hand.__str__()
        # check in hand class that the calculate value method returns
        # an array  return ["FOUR OF A KIND", self, 8]
        self.assertEqual(self.hand.calculate_hand_value()[0], "FOUR OF A KIND")
        print(self.hand.calculate_hand_value()[0] + " == FOUR OF A KIND")
        self.assertEqual(self.hand.calculate_hand_value()[2], 8)

    def test_is_full_house(self):
        self.hand.overwrite_hand(self.full_house)
        self.hand.__str__()
        # check in hand class that the calculate value method returns
        # an array  return ["FULL HOUSE", self, 7]
        self.assertEqual(self.hand.calculate_hand_value()[0], "FULL HOUSE")
        print(self.hand.calculate_hand_value()[0] + " == FULL HOUSE")
        self.assertEqual(self.hand.calculate_hand_value()[2], 7)

    def test_is_flush(self):
        self.hand.overwrite_hand(self.flush)
        self.hand.__str__()
        # check in hand class that the calculate value method returns
        # an array  return ["FLUSH", self, 6]
        self.assertEqual(self.hand.calculate_hand_value()[0], "FLUSH")
        print(self.hand.calculate_hand_value()[0] + " == FLUSH")
        self.assertEqual(self.hand.calculate_hand_value()[2], 6)

    def test_is_straight_flush(self):
        # now I have a hand with a straight flush
        self.hand.overwrite_hand(self.straight_flush)
        self.hand.__str__()
        # check in hand class that the calculate value method returns
        # an array  return ["STRAIGHT FLUSH", self, 9]
        self.assertEqual(self.hand.calculate_hand_value()[0], "STRAIGHT FLUSH")
        print(self.hand.calculate_hand_value()[0] + " == STRAIGHT FLUSH")
        self.assertEqual(self.hand.calculate_hand_value()[2], 9)

    def test_is_straight(self):
        self.hand.overwrite_hand(self.straight)
        self.hand.__str__()
        # check in hand class that the calculate value method returns
        # an array  return ["STRAIGHT", self, 6]
        self.assertEqual(self.hand.calculate_hand_value()[0], "STRAIGHT")
        print(self.hand.calculate_hand_value()[0] + " == STRAIGHT")
        self.assertEqual(self.hand.calculate_hand_value()[2], 5)

    def test_is_threeofakind(self):
        self.hand.overwrite_hand(self.three_of_a_kind)
        self.hand.__str__()
        # check in hand class that the calculate value method returns
        # an array  return ["THREE OF A KIND", self, 6]
        self.assertEqual(self.hand.calculate_hand_value()[0], "THREE OF A KIND")
        print(self.hand.calculate_hand_value()[0] + " == THREE OF A KIND")
        self.assertEqual(self.hand.calculate_hand_value()[2], 4)

    def test_is_twopair(self):
        self.hand.overwrite_hand(self.two_pair)
        self.hand.__str__()
        # check in hand class that the calculate value method returns
        # an array  return ["TWO PAIR", self, 3]
        self.assertEqual(self.hand.calculate_hand_value()[0], "TWO PAIR")
        print(self.hand.calculate_hand_value()[0] + " == TWO PAIR")
        self.assertEqual(self.hand.calculate_hand_value()[2], 3)

    def test_is_pair(self):
        self.hand.overwrite_hand(self.pair)
        self.hand.__str__()
        # check in hand class that the calculate value method returns
        # an array  return ["PAIR", self, 6]
        self.assertEqual(self.hand.calculate_hand_value()[0], "PAIR")
        print(self.hand.calculate_hand_value()[0] + " == PAIR")
        self.assertEqual(self.hand.calculate_hand_value()[2], 2)

    def test_highest_card(self):
        self.hand.overwrite_hand(self.high_card)
        self.hand.__str__()
        # check in hand class that the calculate value method returns
        # an array  return ["HIGH CARD", self, 1]
        self.assertEqual(self.hand.calculate_hand_value()[0], "HIGH CARD")
        print(self.hand.calculate_hand_value()[0] + " == HIGH CARD")
        self.assertEqual(self.hand.calculate_hand_value()[2], 1)

    def test_hand_toString(self):
        # Sprint 2 Implementation..
        pass


