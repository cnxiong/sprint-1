from unittest import TestCase
from django.test import TestCase
from AppATDD.classes.account import AccountID


#  TODO COMPLETED FOR SPRINT I PASSING 100%

class TestAccountID(TestCase):

    def setUp(self):
        self.account = AccountID(username="admin", password="password", online=True)

    def test_constructor(self):
        with self.assertRaises(NameError):
            AccountID(username=None, password="password", online=True)
        with self.assertRaises(NameError):
            AccountID(username="adminX", password=None, online=True)

    def test_go_online(self):
        self.assertEqual((self.account.go_online("admin", "password")), "Admin logged in")
        self.assertEqual((self.account.go_online("admin1", "password")), "Admin logged in")
        self.assertEqual((self.account.go_online("admin2", "password")), "Admin logged in")

    def test_is_online(self):
        self.assertEqual((self.account.is_online("admin")), True)
        self.assertEqual((self.account.is_online("admin1")), True)
        self.assertEqual((self.account.is_online("admin2")), True)

    def test_reset_password(self):
        self.assertEqual((self.account.reset_password("XXX")), "New password XXX set")
        self.assertEqual((self.account.reset_password("password2786")), "New password password2786 set")

    def test_bad_reset_password(self):
        with self.assertRaises(TypeError):
            self.assertEqual(self.account.reset_password(True))

    def test_set_username(self):
        self.assertEqual((self.account.set_username("7U_65")), True)
        self.assertEqual((self.account.set_username("1725g1g")),True)
        self.assertEqual((self.account.set_username("bvbvvxmbnBBB")), True)
        self.assertEqual((self.account.set_username("username")), True)

    def test_bad_set_username(self):
        with self.assertRaises(TypeError):
            self.assertEqual((self.account.set_username(100)), "Incorrect data type entered, please enter a string")

    def test_check_username(self):
        self.assertEqual((self.account.check_username("7U_65")), "7U_65 does not exits")
        self.assertEqual((self.account.check_username("1725g1g")), "1725g1g does not exits")
        self.assertEqual((self.account.check_username("bvbvvxmbnBBB")), "bvbvvxmbnBBB does not exits")
        self.assertEqual((self.account.check_username("username")), "username does not exits")

    def test_bad_check_username(self):
        with self.assertRaises(TypeError):
            self.assertEqual((self.account.check_username(100)), "Incorrect data type entered, please enter a string")