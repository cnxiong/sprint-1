from django.test import TestCase
from unittest import TestCase
from AppATDD.classes import deck
from AppATDD.classes import card

# TODO
#   THESE TESTS ARE DONE AND PASSING USING PYTHON UNITTEST
#  TODO COMPLETED FOR SPRINT I PASSING 100%

class DeckDrawValue(TestCase):
    # 1:clubs 2:spades 3:hearts 4:diamonds
    # initialize a card to the given #suit (1-4) #rank (1-13)

    # Creates a valid deck for testing
    def setUp(self):
        # A new initialized deck should have cards in order
        self.actual_response = deck.DeckClass()
        self.actual_response2 = deck.DeckClass()
        self.actual_response3 = deck.DeckClass()
        # Creates a valid deck for testing
        self.validDeck = []
        for currentSuit in range(1, 5):
            # valid deck creation
            for currentRank in range(1, 14):
                card1 = card.CardClass(currentSuit, currentRank)
                self.validDeck.append(card1)

    def test_draw_valid(self):
        # Deck stored in python array with append()
        # Pop() top card from deck is assumed to be used
        # Expected card drawn is King of Diamonds.
        card = self.actual_response.draw_in_order()
        self.assertEquals(self.validDeck[0].getSuit(), card.getSuit())
        self.assertEquals(self.validDeck[0].getRank(), card.getRank())

    def test_card_drawn_value(self):
        # Draw out Ace of Clubs from ordered deck.
        card_drawn = self.actual_response2.draw_in_order()
        # test card_drawn Rank & Suit.
        self.assertGreaterEqual(1, card_drawn.getRank())
        self.assertLessEqual(1, card_drawn.getSuit())
        self.assertGreaterEqual(1, card_drawn.getSuit())
        self.assertGreaterEqual(1, card_drawn.getRank())
        self.assertLessEqual(1, card_drawn.getSuit())

    def test_Draw_Invalid_Card(self):
        card_drawn = self.actual_response3.draw_in_order()
        expected_card = card.CardClass(1, 1)
        self.assertEqual(card_drawn.__str__(), expected_card.__str__())


class DeckDrawException(TestCase):

    def setUp(self):
        # A new initialized deck should have cards in order
        self.actual_response = deck.DeckClass()
        self.actual_response0 = deck.DeckClass()
        self.deck_class1 = deck.DeckClass()
        # Creates a valid deck for testing
        self.validDeck = []
        for currentSuit in range(1, 5):  # valid deck
            for currentRank in range(1, 14):
                card1 = card.CardClass(currentSuit, currentRank)
                self.validDeck.append(card1)

    def test_Draw_Empty(self):
        for x in range(52):
            self.actual_response.draw()
        # empty out deck and attempt to draw more than 52 cards, ensure this raises an error
        with self.assertRaises(ValueError):
            self.actual_response.draw()

    def test_draw_invalid_exist(self):
        for x in range(51):
            card_drawn = self.actual_response0.draw()
            # i.e. King of Diamonds is drawn from the deck.
            # King of Diamonds should no longer be in the deck
            # if that card is still present Raise and error
            if card_drawn in self.actual_response0.validDeck:
                with self.assertRaises(ValueError):
                    raise ValueError("card drawn is still present in deck.")

    def test_draw_invalid_card(self):
        for x in range(51):
            card_drawn = self.deck_class1.draw()

            # check the card Rank is in range
            if card_drawn.getRank() > 4 | card_drawn.getRank() < 1:
                with self.assertRaises(ValueError):
                    raise ValueError("Card is not in range.")

            # check the card Value is in range
            if card_drawn.getValue() > 13 | card_drawn.getValue() < 1:
                with self.assertRaises(ValueError):
                    raise ValueError("Card is not in range.")

            # If the card drawn is not found in the setup deck then
            # Raise an assertion error because the card pop()
            # Does NOT have a valid value
            if card_drawn not in self.validDeck:
                with self.assertRaises(ValueError):
                    raise ValueError("card drawn is not found in the setup deck.")

class DeckInit(TestCase):
    # Creates a valid deck for testing
    def setUp(self):
        self.actual_response = deck.DeckClass()
        self.validDeck = []
        for currentSuit in range(1, 5):  # valid deck
            for currentRank in range(1, 14):
                card1 = card.CardClass(currentSuit, currentRank)
                self.validDeck.append(card1)

    # Test deck initialization with default constructor
    def test_DeckInit(self):
        for index in range(0, 52):
            self.assertEqual(self.validDeck[index].getRank(), self.actual_response.validDeck[index].getRank())
            self.assertEqual(self.validDeck[index].getSuit(), self.actual_response.validDeck[index].getSuit())

class DeckShuffle(TestCase):
    # Test deck shuffle method for Deck class
    # setup valid deck with card objects
    def setUp(self):
        self.myDeck = deck.DeckClass().shuffle() # shuffle returns a shuffled deck array
        self.validDeck = []
        for currentSuit in range(1, 5):  # valid deck
            for currentRank in range(1, 14):
                cardX = card.CardClass(currentSuit, currentRank)
                self.validDeck.append(cardX)

    # test shuffle method of deck class
    # if shuffle method works, the decks should not be equal
    def test_correct_shuffle(self):
        # shuffle second deck
        self.assertNotEqual(self.validDeck, self.myDeck)  # check that decks are not equal
        differences = 0

        # count the differences between the original and shuffled deck to make sure it shuffled correctly
        for index in (0,51):
            if self.validDeck[index].getSuit() != self.myDeck[index].getSuit():
                differences = differences + 1

            if self.validDeck[index].getRank() != self.myDeck[index].getRank():
                differences = differences + 1

        if differences < 2:
            raise ValueError(f"Deck Not Shuffled Properly! differences = {differences}")

class DeckCount(TestCase):
    # setup valid deck with card objects
    def setUp(self):
        self.actual_response = deck.DeckClass()  # create fake deck until deck.py is finished
        self.actual_response1 = deck.DeckClass()  # create fake deck until deck.py is finished
        self.actual_response2 = deck.DeckClass()  # create fake deck until deck.py is finished
        self.actual_response3 = deck.DeckClass()  # create fake deck until deck.py is finished

        self.validDeck = []
        for currentSuit in range(1, 5):  # valid deck
            for currentRank in range(1, 14):
                card1 = card.CardClass(currentSuit, currentRank)
                self.validDeck.append(card1)

    # test count for proper full deck of 52.
    def test_full_deck(self):
        expected_response = 52
        self.assertEqual(expected_response, self.actual_response1.count)  # check for full deck

    # Test for proper count of deck if all cards are drawn out of the deck.
    def test_empty_deck(self):

        expected_response = 0  # expected response of empty deck

        for x in range(52):  # empty out deck
            self.actual_response.draw()

        self.assertEqual(expected_response, self.actual_response.count)  # check for empty deck

    # test for count less than 0. if deck is empty and an attempt to draw is issued,
    # an exception is thrown.
    def test_non_positive(self):
        for x in range(52):  # empty out deck and draw more than 52 cards
            self.actual_response1.draw()
        with self.assertRaises(ValueError):
            self.actual_response1.draw()

    # test for count greater than 52.
    def test_init_overstock_deck(self):
        # count should be less than or equal to 52.
        self.assertLessEqual(self.actual_response2.count, 52)
        # draw a card from the deck
        self.actual_response2.draw()
        # after drawing, the count should be less than or equal to 52.
        self.assertLessEqual(self.actual_response2.count, 52)

        # Test count of draws from 52 to 0

    def test_init_semi_deck(self):
        expected_response = 52  # expected result

        # initial testing of 52 cards
        self.assertEqual(expected_response, self.actual_response3.count)

        # Fully test the deck for proper count of the deck as
        # cards are drawn from the deck.
        # Each card is individually drawn and individually
        # checked for proper count.
        for x in range(52):
            self.actual_response3.draw()  # draw a card
            expected_response = expected_response - 1  # decrement deck count by 1.
            self.assertEqual(expected_response, self.actual_response3.count)  # check for correct count