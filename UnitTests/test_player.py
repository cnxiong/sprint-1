from AppATDD.classes.hand import HandClass
from AppATDD.classes.deck import DeckClass
from AppATDD.classes.player import PlayerClass
from AppATDD.models import Player, Game

# TODO
#   WIP: Work in Progress SPRINT II IMPLEMENTATION...

class PlayerClass:

    def setUp(self):
        # self.chips = 100
        # self.hand = None
        # self.game_status = None  # game status will be raise,bet,check,fold
        # self.username = "PLAYER1"
        # self.model_player = None
        self.PLAYER1 = PlayerClass("PLAYER1")
        self.PLAYER1 = PlayerClass("PLAYER2")

    def test_new_hand(self):
        self.assertEqual(len(self.PLAYER1.new_hand()), 5)
        self.assertEqual(len(self.PLAYER2.new_hand()), 5)

    def clear_hand(self):
        self.assertEqual((self.PLAYER1.clear_hand()), None)
        self.assertEqual((self.PLAYER2.clear_hand()), None)

    def clear_status(self):
        self.assertEqual((self.PLAYER1.game_status), None)
        self.assertEqual((self.PLAYER2.game_status), None)

    def give_player_winnings(self):
        chips = self.PLAYER1.chips
        pot = 100
        result = chips + pot
        self.assertEqual((self.PLAYER1.give_player_winnings(pot)), result)
        chips = self.PLAYER1.chips
        pot = 100
        result = chips + pot
        self.assertEqual((self.PLAYER2.give_player_winnings(pot)), result)
        # player wins the pot, update player's chips

    def player_buy_in(self):#, amount, _tablename):
        # # return true if player has successfully bought in,
        # # false if they have not
        # if self.chips - amount > 0:
        #     self.chips = self.chips - amount
        #     self.update_model_player_chips(self.chips, "BUY IN")
        #     self.increment_model_game_pot(amount, _tablename)
        #     self.increment_player_amount_bet(amount)
        #     return True
        # else:
        #     return False
        pass

    def player_check(self):#, _tablename):
        # self.game_status = "CHECK"
        # self.update_model_player_chips(self.chips, "CHECK")
        # return
        pass

    def player_call(self):#, amount, _tableName):
        # if self.chips - amount > 0:
        #     self.game_status = "CALL"
        #     self.update_model_player_chips(self.chips, "CALL")
        #     self.increment_model_game_pot(amount, _tableName)
        #     self.increment_player_amount_bet(amount)
        #     return f"Player Call {amount} chips"
        # else:
        #     return f"Player does not have enough chips"
        pass

    def player_bet(self):#, amount, _tablename):
        #
        # # this player can choose to bet
        # if self.chips - amount > 0:
        #     self.chips = self.chips - amount
        #     self.game_status = "BET"
        #     self.update_model_player_chips(self.chips, "BET")
        #     self.increment_model_game_pot(amount, _tablename)
        #     self.increment_player_amount_bet(amount)
        # else:
        #     print("Insufficient Funds")
        pass

    def player_raise(self):#, amount, _tablename):
        # # this player can choose to raise his/her hand
        # if self.chips - amount > 0:
        #     self.chips = self.chips - amount
        #     self.game_status = "RAISE"
        #     self.update_model_player_chips(self.chips, "RAISE")
        #     self.increment_model_game_pot(amount, _tablename)
        #     self.increment_player_amount_bet(amount)
        # else:
        #     print("Insufficient Funds")
        pass

    def test_player_fold(self):
        # # this player can choose to fold his/her hand
        # self.game_status = "FOLD"
        # self.update_model_player_chips(self.chips, "FOLD")
        pass

    def test_get_username(self):
        # return the username of this player
        # return self.username
        pass

    def test_get_coins(self):
        # return the amount of coins this player has
        # return self.chips
        pass

    def show_hand(self):
        # # this will show the current hand that the player has
        # return self.hand.hand_toString()
        pass
