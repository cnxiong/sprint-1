from django.shortcuts import render
from django.views import View
from AppATDD.models import Admin, Player, Table, Game
from AppATDD.classes.command import Command



# Create your views here.

class Home(View):
    myMessage = ''

    def get(self, request):
        return render(request, 'main/index.html', {
            'Admin': Admin.objects.all(),
            'Players': Player.objects.all(),
            'Game': Game.objects.all(),
            'Table': Table.objects.all()
        })

    def post(self, request):
        ui = Command()
        userInput = request.POST["command"]
        print(userInput)
        if userInput:
            myMessage = ui.command(userInput)
        else:
            myMessage = "Invalid commands or No command provided"

        return render(request, 'main/index.html', {
            'Message': myMessage,
            'Admin': Admin.objects.all(),
            'Players': Player.objects.all(),
            'Game': Game.objects.all(),
            'Table': Table.objects.all()
        })
