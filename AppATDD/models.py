from django.db import models
# Create your models here.


GAME_STATUS = {("NONE", "NONE"),("RAISE", "RAISE"), ("BET", "BET"),
    ("CHECK", "CHECK"), ("FOLD", "FOLD"), ("BUY IN", "BUY IN")}

class Admin(models.Model):
    username = models.CharField(max_length=20)
    password = models.CharField(max_length=20)
    is_online = models.BooleanField(default=False)

    def __str__(self):
        return self.username


class Player(models.Model):
    # string filed, required
    username = models.CharField(max_length=20)
    # player money
    chips = models.IntegerField(default=True, blank=True)
    # add to a 5 card hand, or you can just make it a string
    amount_bet = models.IntegerField(default=True, blank=True)
    hand = models.CharField(blank=True, max_length=20)
    hand_value = models.IntegerField(default=True, blank=True)
    #status = inactive, raise, fold, bet, check
    status = models.CharField(max_length=20, default=None, blank=True, choices=GAME_STATUS)

    def __str__(self):
        return self.username

# This model will keep track of the final winner
class Game(models.Model):
    tablename = models.CharField(blank=True, max_length=20)
    pot = models.IntegerField(default=True, blank=True)
    winner = models.CharField(max_length=30, default=None, blank=True)
    game_live = models.BooleanField(default=False)

    def __str__(self):
        return "Game {}".format(self.game_live)

class Table(models.Model):
    tablename = models.CharField(blank=True, max_length=20)
    seats_available = models.IntegerField(default=5, blank=True)
    seats_taken = models.IntegerField(default=0, blank=True)
    players = models.ManyToManyField(Player, default=None, blank=True)
    # game = models.ForeignKey(Game, default=None, blank=True, on_delete=models.CASCADE)

    def __str__(self):
        return "{}".format(self.tablename)