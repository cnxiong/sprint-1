from django.contrib import admin
from AppATDD.models import Table, Game, Admin, Player

# Register your models here.
admin.site.register(Admin)
admin.site.register(Player)
admin.site.register(Game)
admin.site.register(Table)