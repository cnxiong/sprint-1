from AppATDD.classes.account import AccountID
from AppATDD.classes.player import PlayerClass
from AppATDD.models import Admin

# import queue

class AdminClass(AccountID):

    def __init__(self, user, password, is_online):

        if user is None or password is None or is_online is None:
            raise NameError("invalid entry for admin account")
        if not isinstance(user, str) and not isinstance(password, str):
            raise TypeError("Incorrect data type entered, please enter a string")
        if not isinstance(is_online, bool):
            raise TypeError("Incorrect data type entered for is_online")

        self.wait_list = []
        self.tables = []
        self.tableNumbers = []
        AccountID.__init__(self, username=user, password=password, online=is_online)

    def get_model_object(self, user_id):
        # Just return one Admin in case there is a glitch and multiple are returned.
        try:
            admin = Admin.objects.get(username__exact=user_id)
            return admin
        except:
            return None

    def create_new_player(self, username, admin_id):
        # change this logic, players will be removed from waitlist during games..
        if self.wait_list.__contains__(PlayerClass(username)):
            # if account is already exist, ask for a new account
            return f"username {username} already exists, please try a new account name"
        else:
            # dealers are able to create new players with user name only
            # new players by default are added to the wait list
            self.wait_list.append(PlayerClass(username))
            return f"username {username} successfully created"
