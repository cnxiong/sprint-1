from AppATDD.classes.deck import DeckClass
from AppATDD.models import Table
from AppATDD.models import Game
from AppATDD.models import Player

# TODO ----CONNECT GAME CLASS TO MODELS ---
# Use the game class to connect everything to models

# The table class will track the pot, and bets game will track the hands
# and winners

class GameClass():
    def __init__(self, players, tablename):

        if players is None or tablename is None:
            raise NameError("invalid entry for GameClass")
        if not isinstance(tablename, str):
            raise TypeError("Incorrect data type entered, please enter a string")

        self.pot = 0
        self.buy_in = 50
        self.tablename = tablename
        self._winner = None
        self.game_in_progress = True
        self.deck = DeckClass().shuffle()
        self.players_in_game = players
        self.create_game_model()
        # players are in the Table, and hands are connected to players
        # Create a game model...
        # may need to add a more specific key to Game like table name etc.

    def start(self):
        if self.game_model_exists():
            Game.objects.filter(tablename=self.tablename).update(winner=" ", pot=0)

        # this will initialize a new game and generate hands for all players
        for player in self.players_in_game:
            player.clear_player_amount_bet()
            # buy_in() will return true if player had the money to join
            # buy_in will also update the pot and decrement player funds
            if player.player_buy_in(self.buy_in,self.tablename):
                player.new_hand()
            else:
                player.player_fold()
        self.game_progress = True

    def get_high_hand(self):
        # THIS METHOD helper_get_high_hand REQUIRES CLASS LOGIC SET-UP, will simplify command.py to incorporate for Sprint II
        # _winner = self.helper_get_high_hand(self.players_in_game)
        _winner = " "
        value = 0
        if Table.objects.filter(tablename=self.tablename).exists():
            getTable = Table.objects.get(tablename__exact=self.tablename)
            if getTable.players.all():
                for player in getTable.players.all():
                    field_name = "hand_value"
                    pot = getattr(player, field_name)
                    if pot > value:
                        value = pot
                        _winner = player
        return _winner  # return the player with the highest hand value.

    def total_pot(self):
        return self.pot  # return the total pot

    def update_total_pot(self, coins):
        self.pot = self.pot + coins

    def update_winner_message(self, player):
        string = " "
        self._winner = player  # update who wins the pot
        sb = [self._winner.username]#, player.hand.get_hand_value()[0]]
        # winner.hand a nonetype object issue, I think it has to do with a player folding.
        string = ' '.join(sb)
        return string

    def game_over(self):
        # find the winner and the highest hand
        winner = self.get_high_hand()

        if winner == None:
            return print("ERROR WINNER = NONE ...")

        message = self.update_winner_message(self.get_high_hand())
        print(message)
        winner_username = self._winner.username

        if self.game_model_exists():
            Game.objects.filter(tablename=self.tablename).update(winner=message)
            # --------UPDATE PLAYER MONEY TO INCLUDE WINNINGS---------------------
            winnings = Player.objects.get(username=winner_username).chips + self.pot
            Player.objects.filter(username=winner_username).update(chips=winnings)
            # ---------------------------------------------------------------------
        self.display()
        self.game_in_progress = False

    def display(self):
        # this will display the total pots and the winner for the game
        print("Winner:" + self.update_winner_message(self._winner))
        print("Pot:" + self.pot.__str__())


    # GAME MODEL CLASS METHODS ============================================================
    def create_game_model(self):
        if not Game.objects.filter(tablename=self.tablename).exists():
            self.model_game = Game.objects.create(tablename=self.tablename,pot=self.pot, winner=" ", game_live=False)
        else:
            self.model_game = Game.objects.filter(tablename=self.tablename)

    def model_game_is_live(self, boolean):
        pass

    def get_players_in_game_model(self):
        player_models_in_game = []
        if self.game_model_exists():
            if self.table_model_exists():
                for player in Table.objects.filter(tablename=self.tablename).all():
                    player_models_in_game.append(player)
        return player_models_in_game

    def game_model_exists(self):
        if Game.objects.filter(tablename=self.tablename).exists():
            return True
        return False

    def table_model_exists(self):
        if Table.objects.filter(tablename=self.tablename).exists():
            return True
        return False


    # # DETERMINATION WINNING HAND ===============================================
    # def is_h1_greater_than_h2(self, v1, v2):
    #     # TODO SPRINT II TBD
    #     return True
    #
    # def helper_get_high_hand(self, players):
    #     # THIS LOGIC IS LOCAL, most pass in player objects with pre-determined hands.
    #     highest = 0
    #     print("players.... ")
    #     for player in players:
    #         print(player.username)
    #         _winner = player
    #         if player.hand != None:
    #             if player.game_status != "FOLD":
    #                 value = player.hand.get_hand_value()[2]  # = value 1-10
    #                 print(value)
    #                 print(player.hand.get_hand_value()[2])
    #                 if value >= highest:
    #                     highest = value
    #                     _winner = player
    #                 if value == highest:
    #                     v1 = player.hand.get_hand_value()[1]
    #                     v2 = _winner.hand.get_hand_value()[1]
    #                     if self.is_h1_greater_than_h2(v1, v2):
    #                         # if the next hand is bigger update values
    #                         highest = value
    #                         _winner = player
    #         return _winner  # return the player with the highest hand value.
    #     return None