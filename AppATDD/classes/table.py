from AppATDD.classes.game import GameClass
from AppATDD.classes.player import PlayerClass
from AppATDD.models import Table
from AppATDD.models import Game

# TODO -- connect Table to Models -----
# Fully connect the game to models this will be your main class.
# Test all your class logic in Command.py demo()

class TableClass():

    def __init__(self, players, tablename):
        self.buy_in = 50
        self.SEATS_AVAILABLE = 5
        self.seats_taken = 0
        self.table_name = tablename
        self.table_players = players
        self.model_table = None
        self.game = None
        self.create_table_model()

    def get_buy_in(self):
        return self.buy_in

    def get_table_name(self):
        return self.table_name

    def get_players(self):
        return self.table_players

    def remove_player(self, removeplayer):
        # CHECK THE PLAYER IS AT THE TableClass Table
        if self.get_players().__contains__(removeplayer):
            self.table_players.remove(removeplayer)
            self.seats_taken = self.seats_taken - 1
            # CHECK THE PLAYER IS AT THE Table Model Table
            if self.table_model_exists(self.table_name):
                for current_player in self.model_table.players.all():
                    if current_player.username == removeplayer.username:
                        self.model_table.players.remove(removeplayer.get_model_object())
                        Table.objects.filter(tablename=self.table_name).update(seats_taken=self.seats_taken)
            else:
                print("Error player not at table")
        else:
            print("Error Table Empty")

    def add_player_to_table(self, addplayer):
        # CHECK THERE IS SPACE AT THE TABLE
        if self.seats_taken < 5:
            # CHECK THE PLAYER IS NOT AT THE TableClass Table
            if not self.get_players().__contains__(addplayer):
                self.table_players.append(addplayer)
                self.seats_taken = self.seats_taken + 1
                # CHECK THE PLAYER IS NOT AT THE Table Model Table
                if self.table_model_exists(self.table_name):
                    for current_player in self.model_table.players.all():
                        if not current_player.username == addplayer.username:
                            # TODO FIX.. don't add more than 5 players so model table
                            # # GET THE SEATS TAKEN AT THE Table Model Table
                            # field_name = 'seats_taken'
                            # game = Game.objects.get(tablename=self.table_name)
                            # model_seats_taken = getattr(game, field_name)
                            # # UPDATE THE SEATS TAKEN AT THE Table Model Table
                            # if model_seats_taken < 5: # Update model seats taken
                                # ADD PLAYER TO A SEAT AT THE Table Model Table
                            self.model_table.players.add(addplayer.get_model_object())
                            Table.objects.filter(tablename=self.table_name).update(seats_taken=self.seats_taken)
            else:
                print("Error player already at table")
        else:
            print("Error Table at Capacity")

    def start_a_game(self):
        self.game = GameClass(self.table_players)
        for player in self.table_players:
            # return True is player has successfully bought into the game
            if player.player_buy_in(self.buy_in):
                self.game.update_total_pot(self.buy_in)
                # add to the pot for each player joining the game
            else:
                self.table_players.remove(player)
                print("player: " + player + " is too broke to but in and was kicked out of the table")
        # The game class deals your cards tracks the winner and who gets the pot
        self.game.start()

    def end_a_game(self):
        self.game = self.game.game_over()

    def check_if_table_is_at_capacity(self):
        return self.seats_taken >= 5


    # MODEL CLASS METHODS ============================================================
    def create_table_model(self):
        if not Table.objects.filter(tablename=self.table_name).exists():
            self.model_table = Table.objects.create(tablename=self.table_name,seats_available=5,seats_taken=0)
            for player in self.table_players:
                # add all players to the table
                # model_player = player.get_model_object()
                self.model_table.players.add(player.get_model_object())
        else:
            self.model_table = Table.objects.get(tablename__exact=self.table_name)

    def table_model_exists(self,table_name):
        if Table.objects.filter(tablename=table_name).exists():
            return True
        return False

    def get_model_object(self):
        if Table.objects.filter(tablename=self.table_name).exists():
            return Table.objects.get(tablename__exact=self.table_name)
    #  ============================================================


