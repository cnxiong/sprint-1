from collections import Counter
from AppATDD.classes.deck import DeckClass

class HandClass():
    def __init__(self, deck):
        if deck is None:
            raise ValueError("deck is none!")
        if not isinstance(deck, DeckClass):
            raise TypeError("you must enter DeckClass for deck")

        # array of Card(suit,rank) objects, check card class
        self.cards = []
        self.new_hand(deck)
        self.hand_value = self.calculate_hand_value()

    def get_hand_value(self):
        return self.hand_value

    def new_hand(self, deck):
        if deck.count < 5:
            deck = DeckClass()
        self._add_card(deck)
        self._add_card(deck)
        self._add_card(deck)
        self._add_card(deck)
        self._add_card(deck)

    def _add_card(self, Deck):
        # choose the add a card from the Table's deck to the hand.
        self.cards.append(Deck.draw())

    def overwrite_hand(self, cards):
        self.empty_hand()
        self.hand_value = 0

        for card in cards:
            self.cards.append(card)

        if self.cards.__len__() == 5:
            self.hand_value = self.calculate_hand_value()
        else:
            raise ValueError("must use 5 cards to overwrite hand")

    def sortRank(self):
        # 1:clubs 2:spades 3:hearts 4:diamonds
        # initialize a card to the given #suit (1-4) #rank (1-13)
        ret = []
        min = self.cards[0].getRank()
        for card in self.cards:
            if min > card.getRank():
                min = card.getRank()
        # now min Rank is determined, use this to sort
        while ret.__len__() < 5:
            for card in self.cards:
                if min >= card.getRank():
                    if not ret.__contains__(card):
                        ret.append(card)
            min = min + 1
        self.cards = ret
        return ret

    def sortSuit(self):
        # 1:clubs 2:spades 3:hearts 4:diamonds
        # initialize a card to the given #suit (1-4) #rank (1-13)
        ret = []
        min = self.cards[0].getSuit()
        for card in self.cards:
            if min > card.getSuit():
                min = card.getSuit()
        # now min Suit is determined, use this to sort
        while ret.__len__() < 5:
            for card in self.cards:
                if min >= card.getSuit():
                    if ret.__contains__(card) == False:
                        ret.append(card)
            min = min + 1
        self.cards = ret
        return ret

    def empty_hand(self):
        self.cards = []
        self.hand_value = ["NONE",None,0]

    def __str__(self):
        # list all cards in the hand ie "K H, A D, 10 C"
        # maybe include a print statement for like full house or two of a kind?
        print("--------")
        sb = []
        for card in self.cards:
            sb.append(card.__str__())
        string = ' '.join(sb)
        print(string)
        return string

    def is_royal(self):
        self.cards = self.sortSuit()
        # after sorting if index 0 == 4 this means from the
        # first and last all are the same suit
        if self.cards[0].getSuit() == self.cards[4].getSuit():
            self.cards = self.sortRank()
            # now after rank is sorted..
            # if your first two cards are Ace and 10 the last
            # three have to be higher in order and the same suit
            if self.cards[0].getRank() == 1 and self.cards[1].getRank() == 10:
                return True
        return False

    def is_fourofakind(self):
        four_pair = []
        self.cards = self.sortRank()
        # when 4 of a kind is sorted either the last 4
        # or the first 4 will match, so let's take out
        # the first or 4th index when comparing
        if self.cards[0].getRank() == self.cards[1].getRank():
            rank = self.cards[0].getRank()
        else:
            rank = self.cards[4].getRank()

        for card in self.cards:
            # now check for 4 with matching numbers
            # and add them to a list of 4 cards
            if rank == card.getRank():
                if not four_pair.__contains__(card):
                    four_pair.append(card)

        if four_pair.__len__() == 4:
            return [True, four_pair]
        return [False, None]

    def is_fullhouse(self):
        self.cards = self.sortRank()
        # sorted by rank I have 2 of a kind and 3 of a kind,
        # which means the first two and last 2 would be equal
        # then I should check my middle card to make sure it equals the first or last
        if self.cards[0].getRank() == self.cards[1].getRank()and self.cards[3].getRank()== self.cards[4].getRank():
            if self.cards[2].getRank() == self.cards[0].getRank() or self.cards[2].getRank()== self.cards[4].getRank():
                return True
        return False

    def is_flush(self):
        self.cards = self.sortSuit()
        # check if all cards are the same suit
        for Suit in range(1, 5):
            count = 0
            for card in self.cards:
                if card.getSuit() == Suit:
                    count = count + 1
                if count == 5:
                    return True
        # Test this method at the end so that the methods
        # previous to this have already checked for pairs
        # and sequences
        return False

    def is_straight_flush(self):
        count = 0
        prev_card = self.cards[0]
        self.cards = self.sortRank()
        # check all are in ascending order
        for card in self.cards:
            if prev_card.getRank() + 1 == card.getRank():
                # count should be 4 for 5 cards in order
                # card[0] will not register
                count = count + 1
            prev_card = card
        if count == 4:
            return True
        return False

    def is_straight(self):
        count = 0
        prev_card = self.cards[0]
        self.cards = self.sortRank()
        # check all are in ascending order
        for card in self.cards:
            if prev_card.getRank() + 1 == card.getRank():
                # count should be 4 for 5 cards in order
                # card[0] will not register
                count = count + 1
            prev_card = card
        if count == 4:
            return True
        return False

    def is_threeofakind(self):
        self.cards = self.sortRank()
        for Rank in range(1, 14):
            count = 0
            three_kind = []
            for card in self.cards:
                if card.getRank() == Rank:
                    count = count + 1
                    three_kind.append(card)
                if count == 3:
                    return [True, three_kind]
            # Test this method after 4 of a kind.
        return [False, None]

    def is_twopair(self):
        self.cards = self.sortRank()
        counts = []
        ret_val = []
        for Rank in range(1, 14):
            count = 0
            two_pair = []
            for card in self.cards:
                if card.getRank() == Rank:
                    count = count + 1
                    two_pair.append(card)
                if count == 2:
                    counts.append(card.getRank())
                    ret_val.append(two_pair)
                    break
                if counts.__len__() == 2:
                    return [True, ret_val]
        return [False, None]

    def is_pair(self):
        count = 0
        pair = []
        prev_card = self.cards[0]
        self.cards = self.sortRank()
        # check all are in ascending order
        for card in self.cards:
            if prev_card.getRank() == card.getRank():
                if count > 0:
                    # card[0] will equal card[0]
                    # so dis-count this count to start
                    # if there is a match beyond the first card with itself
                    # at least two pair
                    pair.append(prev_card)
                    pair.append(card)
                count = count + 1
            prev_card = card
        if count == 2:
            return [True, pair]
        return [False, pair]

    def highest_card(self):
        self.cards = self.sortRank()
        return self.cards[4]

    # add another method to evaluate the pairs & doubles

    def calculate_hand_value(self):
        # use self.cards and calculate what the list of cards is worth.
        # calculate the value of the cards in hand
        # you will need a full system to accurately value all hands

        if self.is_royal():
            return ["ROYAL FLUSH", self, 10]

        elif self.is_straight_flush() and self.is_flush():
            return ["STRAIGHT FLUSH", self, 9]

        elif self.is_fourofakind()[0]:
            four_kind = self.is_fourofakind()[1]
            return ["FOUR OF A KIND", four_kind, 8]

        elif self.is_fullhouse():
            return ["FULL HOUSE", self, 7]

        elif self.is_straight():
            return ["STRAIGHT", self, 5]

        elif self.is_threeofakind()[0]:
            three_kind = self.is_threeofakind()[1]
            return ["THREE OF A KIND", three_kind, 4]

        elif self.is_twopair()[0]:
            two_pair = self.is_twopair()[1]
            return "TWO PAIR", two_pair, 3

        elif self.is_pair()[0]:
            pair = self.is_pair()[1]
            return ["PAIR", pair, 2]

        elif self.is_flush():
            return ["FLUSH", self, 6]

        else:
            return ["HIGH CARD", self.highest_card(), 1]

        # like sort based on number or sort based on suit. This class is a lot of logic
        # will need to check if full house two of a kind, three of a kind etc
        # assign a point system
        # Tip maybe look for array sort methods? use some logic to calculate hand values