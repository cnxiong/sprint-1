from AppATDD.classes.hand import HandClass
from AppATDD.classes.deck import DeckClass
from AppATDD.models import Player, Game

class PlayerClass:
    def __init__(self, player_username):
        self.chips = 100
        self.hand = None
        self.game_status = None  # game status will be raise,bet,check,fold
        self.username = player_username
        self.model_player = None
        self.create_player_model()

    def new_hand(self):
        self.hand = HandClass(DeckClass())
        # value2 = self.hand.get_hand_value()[1]
        # highest = 0
        # for value in value2:
        #     new_highest = value
        value = self.hand.get_hand_value()[2]
        str = self.hand.get_hand_value()[0]+": "+self.hand.__str__()
        if self.player_model_exists():
            Player.objects.filter(username=self.username).update(hand=str)
            Player.objects.filter(username=self.username).update(hand_value=value)
        return self.hand

    def clear_hand(self):
        self.hand = None
        if self.player_model_exists():
            Player.objects.filter(username=self.username).update(hand=" ")
        return self.hand

    def clear_status(self):
        self.game_status = None
        if self.player_model_exists():
            Player.objects.filter(username=self.username).update(status="NONE")
        return self.hand

    def give_player_winnings(self, pot):
        # player wins the pot, update player's chips
        self.chips = self.chips + pot
        try:
            self.update_model_player_chips(self.chips, "NONE")
        except:
            raise LookupError("this Player does not exist, they cannot win")

    def player_buy_in(self, amount, _tablename):
        # return true if player has successfully bought in,
        # false if they have not
        if self.chips - amount > 0:
            self.chips = self.chips - amount
            self.update_model_player_chips(self.chips, "BUY IN")
            self.increment_model_game_pot(amount, _tablename)
            self.increment_player_amount_bet(amount)
            return True
        else:
            return False

    def player_check(self, _tablename):
        self.game_status = "CHECK"
        self.update_model_player_chips(self.chips, "CHECK")
        return

    def player_call(self, amount, _tableName):
        if self.chips - amount > 0:
            self.game_status = "CALL"
            self.update_model_player_chips(self.chips, "CALL")
            self.increment_model_game_pot(amount, _tableName)
            self.increment_player_amount_bet(amount)
            return f"Player Call {amount} chips"
        else:
            return f"Player does not have enough chips"

    def player_bet(self, amount, _tablename):

        # this player can choose to bet
        if self.chips - amount > 0:
            self.chips = self.chips - amount
            self.game_status = "BET"
            self.update_model_player_chips(self.chips, "BET")
            self.increment_model_game_pot(amount, _tablename)
            self.increment_player_amount_bet(amount)
        else:
            print("Insufficient Funds")

    def player_raise(self, amount, _tablename):
        # this player can choose to raise his/her hand
        if self.chips - amount > 0:
            self.chips = self.chips - amount
            self.game_status = "RAISE"
            self.update_model_player_chips(self.chips, "RAISE")
            self.increment_model_game_pot(amount, _tablename)
            self.increment_player_amount_bet(amount)
        else:
            print("Insufficient Funds")

    def player_fold(self):
        # this player can choose to fold his/her hand
        self.game_status = "FOLD"
        self.update_model_player_chips(self.chips, "FOLD")

    def get_username(self):
        # return the username of this player
        return self.username

    def get_coins(self):
        # return the amount of coins this player has
        return self.chips

    def __str__(self):
        return self.username

    def show_hand(self):
        # this will show the current hand that the player has
        return self.hand.hand_toString()

    # PLAYER MODEL CLASS METHODS ============================================================

    def create_player_model(self):
        # create a new player models object every-time you instantiate a new player
        if not Player.objects.filter(username=self.username).exists():
            self.model_player = Player.objects.create(username=self.username, chips=self.chips, status="NONE")
        else:
            self.model_player = Player.objects.filter(username__exact=self.username)

    def player_model_exists(self):
        if Player.objects.filter(username=self.username).exists():
            return True
        return False

    def game_model_exists(self, _tablename):
        if Game.objects.filter(tablename=_tablename).exists():
            return True
        return False

    def game_(self, amount, new_status):
        # DEDUCT PLAYER CHIPS
        if self.player_model_exists():
            Player.objects.filter(username=self.username).update(chips=amount, status=new_status)

    def get_model_object(self):
        if Player.objects.filter(username=self.username).exists():
            # need to get a singular model player object
            return Player.objects.get(username__contains=self.username)
        return None

    def increment_player_amount_bet(self, amount):
        # INCREMENT GAME POT
        if self.player_model_exists():
            # the the value of the pot field from models
            field_name = 'amount_bet'
            obj = Player.objects.get(username__exact=self.username)
            bet = getattr(obj, field_name)
            # then update the field
            bet = bet + amount
            # then update the game
            Player.objects.filter(username__exact=self.username).update(amount_bet=bet)

    def clear_player_amount_bet(self):
        # INCREMENT GAME POT
        if self.player_model_exists():
            Player.objects.filter(username__exact=self.username).update(amount_bet=0)

    def increment_model_game_pot(self, amount, _tablename):
        # INCREMENT GAME POT
        if self.game_model_exists(_tablename):
            # the the value of the pot field from models
            field_name = 'pot'
            obj = Game.objects.get(tablename__exact=_tablename)
            pot = getattr(obj, field_name)
            # then update the field
            pot = pot + amount
            # then update the game
            Game.objects.filter(tablename=_tablename).update(pot=pot)

    def update_model_player_chips(self, amount, new_status):
        # DEDUCT PLAYER CHIPS
        if self.player_model_exists():
            Player.objects.filter(username=self.username).update(chips=amount, status=new_status)
    #  ============================================================
