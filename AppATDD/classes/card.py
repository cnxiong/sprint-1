class CardClass:
    def __init__(self, s, r):
        # throw an exception for invalid argument
        if s is None or r is None:
            raise ValueError("please enter both a suit and rank to create a card")
        if not isinstance(s, int) or not isinstance(r, int):
            raise TypeError("you must enter integer fields only for suit and rank")
        if s > 4 or s < 1 or r > 13 or r < 1:
            raise ValueError("you entered a suit and rank out of range")

        # initialize a card to the given
        # suit (1-4) #rank (1-13)
        self.s = s
        self.r = r
        self.value = 0
        # 1:clubs 2:spades 3:hearts 4:diamonds

    def getSuit(self):
        # return the suit of the card (1-4)
        return self.s

    def getRank(self):
        # return the rank of the card (1-13)
        return self.r

    def getValue(self):
        # get the game-specific value of a Card
        # if not used, or game is not defined, always returns 0.
        return self.value

    def __str__(self):
        # return rank and suite, as AS for ace of spades, or 3H for
        # three of hearts, JC for jack of clubs.

        # just added the Joker as a placeholder for the 0 index.
        Suits = ["JOKER","C", "S", "H", "D"]
        Ranks = ["JOKER","A","2","3","4","5","6","7","8","9","10","J","Q","K"]

        # the numbers s, and r should be the indexes in these arrays
        return f"{Ranks[self.r]}{Suits[self.s]}"
        #just fstring python syntax to return a string you can put variables in.
